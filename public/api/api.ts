import axios from 'axios'

export default function useApi() {
    const apiUrl: string = 'https://rickandmortyapi.com/api/'


async function getAllCharacters() {
    return await axios.get(`${apiUrl}character`)
}
async function getPage(page:number,name:string, status:string) {
    return await axios.get(`${apiUrl}character/?page=${page}&name=${name}&status=${status}`)
}
async function filterByNameAndStatus(name:string, status:string) {
    return await axios.get(`${apiUrl}character/?name=${name}&status=${status}`)
}


return {
    getAllCharacters,
    getPage,
    filterByNameAndStatus
}
}